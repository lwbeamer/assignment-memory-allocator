#include <stdio.h>
#include "mem.h"
#include "mem_internals.h"


void test1();
void test2();
void test3();
void test4();
void test5();

void* heap = NULL;
void heap_create(size_t size) {
	heap = heap_init(size);
	debug_heap(stdout, heap);
	fprintf(stdout, "Куча инициализирована\n\n");
}

void test1() {
    void* temp = _malloc(1000);

    if (!temp) {
        fprintf(stderr, "Ошибка в тесте 1!\n\n");
        return;
    }
    
    debug_heap(stdout, heap);
    _free(temp);
    debug_heap(stdout, heap);
    fprintf(stdout, "\nТест 1 пройден!\n\n");
}

void test2() {
    void* temp1 = _malloc(1000);
    void* temp2 = _malloc(2000);

    if (!temp1 || !temp2) {
        fprintf(stderr, "Ошибка в тесте 2!\n\n");
        return;
    }
    
    debug_heap(stdout, heap);
    fprintf(stdout, "\nОсвобождение первого блока...\n");
    _free(temp1);
    debug_heap(stdout, heap);	
    fprintf(stdout, "\nТест 2 пройден!\n\n");
}

void test3() {
    void* temp1 = _malloc(1000);
    void* temp2 = _malloc(2000);
    void* temp3 = _malloc(4000);

    if (!temp1 || !temp2 || !temp3) {
        fprintf(stderr, "Ошибка в тесте 3!\n\n");
        return;
    }

    debug_heap(stdout, heap);
    fprintf(stdout, "\nОсвобождение первого блока...\n");
    _free(temp1);
    debug_heap(stdout, heap);
    fprintf(stdout, "\nОсвобождение второго блока...\n");
    _free(temp2);
    debug_heap(stdout, heap);
    fprintf(stdout, "\nТест 3 пройден!\n\n");
}

void test4() {
    void* temp1 = _malloc(1000);
    void* temp2 = _malloc(2000);
    void* temp3 = _malloc(10000);

    if (!temp1 || !temp2 || !temp3) {
        fprintf(stderr, "Ошибка в тесте 4!\n\n");
        return;
    }

    debug_heap(stdout, heap);
    fprintf(stdout, "\nТест 4 пройден!\n\n");
}

void test5() {
    void* temp1 = _malloc(8000);
    void* temp2 = _malloc(20000);
    void* temp3 = _malloc(100000);

    if (!temp1 || !temp2 || !temp3) {
        fprintf(stderr, "Ошибка в тесте 5!\n\n");
        return;
    }
    
    debug_heap(stdout, heap);
    fprintf(stdout, "\nТест 5 пройден!\n\n");
}

int main() {
    heap_create(10000);
    printf("Тест 1:\n");
    test1();
    printf("Тест 2:\n");
    test2();
    printf("Тест 3:\n");
    test3();
    printf("Тест 4:\n");
    test4();
    printf("Тест 5:\n");
    test5();
    return 0;
}

